## Asset Description :
- This asset contains 1 Lightning Aura component and 2 Apex Classes.
- `EmailVerificationComponent` Lightning Aura Component contains the logic which sends the verification code to the input Email field.
- `EmailOTPService` Apex Class consists the logic to generate verification code.
- `RegisterEmailServiceHandler` Apex Class consists the logic to send email via apex with the verification code.
- This has been used in projects where custom sign up registeration is required (eg. 7s nVent B2B Commerce Cloud Implementation) .

## Asset Use :
- This aura component can be used whenever a custom verification via email is required.