({
    doInit: function(component, event, helper) {
         
    },
    

    handleSendEmail : function(component, event, helper){
        component.set("v.verificationComp" , true);
        component.set("v.mainComp" , false);
        var email = component.find("email").get("v.value");

        component.set("v.emailAddress" , email);

        var emailToSendOTP = component.get("v.emailAddress");   
        console.log('OTP SEND To - ',emailToSendOTP);
        
        var action = component.get('c.sendOTP');
        action.setParams({
            emailAddress : emailToSendOTP
        }); 
        action.setCallback(this, function(response){
                var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                //component.set('v.sObjList', a.getReturnValue());
                var otpSent = response.getReturnValue();
                component.set('v.otpCodeSent', otpSent);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
                console.log('OTP SENT - ERROR - ',errors[0].message);

            }
        });
        $A.enqueueAction(action); 
           
    },

    handleOnChange : function(component, event, helper) {
        var otpValueEntered = component.get("v.otpCodeEntered");;

        if(event.target.name === "otpCodeEntered"){
            component.set("v.otpCodeEntered" , otpValueEntered);    
        }
        
    },
    
    handleVerification : function(component, event, helper) {
        var otpCodeEntered = component.get("v.otpCodeEntered");
        var otpCodeSent = component.get("v.otpCodeSent");

        if( otpCodeSent == otpCodeEntered) {
            // Verification is successful.
            // Set the input to Blank
            component.set("v.otpCodeEntered" , "");    

            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success',
                message: 'Successfully Verified!',
                type: 'success',
                mode: 'dismissible '
            });
            toastEvent.fire();
            
        }
        else {
            console.log("FAILED , NOT VERIFIED");
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: 'Please enter the valid verification code',
                    type: 'error',
                    mode: 'dismissible '
                });
                toastEvent.fire();
        }
    },
    
    handleCancelVerification : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "",
            "isredirect" :false
        });
        urlEvent.fire();

    },

    handleResendCode : function(component, event, helper) {
        var emailToSendOTP = component.get("v.emailAddress");   
        console.log('OTP SEND To - ',emailToSendOTP);
        
        var action = component.get('c.sendOTP');
        action.setParams({
            emailAddress : emailToSendOTP
        }); 
        action.setCallback(this, function(response){
         	var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                //component.set('v.sObjList', a.getReturnValue());
                var otpSent = response.getReturnValue();
                console.log('OTP SENT - SUCCESS - ',otpSent);
                component.set('v.otpCodeSent', otpSent);
 
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
                console.log('OTP SENT - ERROR - ',errors[0].message);
            }
        });
        $A.enqueueAction(action); 
    }
})
