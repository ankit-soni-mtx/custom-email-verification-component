/**
 * CreatedBy : ankitSoni 10/15/2020
 * @description Used for sending Email with verification code.
 */
public class RegisterEmailServiceHandler {
    
    /**
     * @description Will generate, send and return the OTP.
     * @param emailAddress 
     * @return OTP 
     */
	@AuraEnabled
    public static String sendOTP(String emailAddress) {
        try {
            // Generate OTP
            EmailOTPService otp = new EmailOTPService();
            String verificationCode  = otp.getOtp(6);
            
            // Create email with verification code
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String[] toEmails = new string[] {emailAddress};

            email.setToAddresses(toEmails);
            email.setReplyTo('noreply@companyDomain.com'); 
            email.setSubject('Verify your new user account in Company User Portal');
            email.setSenderDisplayName('Company User Portal');
            String emailBody = '';
            emailBody += 'Hello';
            emailBody += '<br/><br/>';
            emailBody += 'You recently attempted to register a new Company User Portal account.';
            emailBody += '<br/><br/>';
            emailBody += 'To confirm your account, let\'s verify your identity. Enter the following code where prompted.';
            emailBody += '<br/><br/>';
            emailBody += 'Verification Code: '+verificationCode;
            emailBody += '<br/><br/>';
            email.setHtmlBody(emailBody);
            
            // Send Email
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {email};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            System.debug('sendOTP - '+results[0].isSuccess());

            return verificationCode;

        } catch(exception e){
            throw new AuraHandledException(e.getMessage());
        }
        
    }
}