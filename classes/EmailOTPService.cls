/** 
 * CreatedBy : ankitSoni 10/15/2020
 */
public class EmailOTPService {
    
    public class EmailOTPServiceException extends Exception {}
	
    private static Integer OTP_LENGTH;
    private static Integer DEFAULT_OTP_LENGTH = 6;
    
    private Integer getOtpLength() {
        if(OTP_LENGTH == null || OTP_LENGTH < 1) {
            OTP_LENGTH = DEFAULT_OTP_LENGTH;
        }
        return OTP_LENGTH;
    }
    
    public void setOtpLength(Integer otpLength) {
        if(otpLength != null && otpLength > 0) {
            OTP_LENGTH = otpLength;
        }
    }
    
    private String generateOtp() {
        Integer multiplier = (Integer) Math.pow(10, this.getOtpLength());
        Integer randomInteger = Math.round(Math.random() * multiplier);
        if(randomInteger == 0) {
            randomInteger = Math.round(Math.random() * multiplier) + 1;
        }
        String new_otp = String.valueOf(randomInteger);
        if(new_otp.length() < this.getOtpLength()) {
            new_otp = new_otp.rightPad(this.getOtpLength(), '5');
        }
        return new_otp;
    }
    
    /**
     * @description Will generte and return OTP of defult length.
     */
    public String getOtp() {
        return getOtp(DEFAULT_OTP_LENGTH);
    }
    
    /**
     * @description Will generte and return OTP.
     */
    public String getOtp(Integer otpLength) {
        try {
            this.setOtpLength(otpLength);
        	return generateOtp();
        } catch(Exception e) {
            System.debug(e.getStackTraceString());
            throw new EmailOTPServiceException(e.getMessage());
        }
    }
}